package com.devcamp.j01_javabasic.s50;

public class Voucher {
    public String voucherCode = "V00000";

    public String getVoucherCode(){
        return voucherCode;
    }
    
    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }
    
    public void showVoucher(){
        System.out.println("Voucher Code is: " + this.voucherCode);
    }
    public void showVoucher(String voucherCode){
        System.out.println("This is voucher code : " + this.voucherCode);
        System.out.println("This is voucher code too: " + voucherCode);
    }

    public static void main(String[] args) throws Exception{
        Voucher voucher = new Voucher();
        
        // voucher.showVoucher();
        // voucher.showVoucher("CODE8888");

        // voucher.voucherCode = "CODE2222";
        // voucher.showVoucher("00008888");

        String code = "VOUCHER";
        voucher.setVoucherCode(code);
        System.out.println(voucher.getVoucherCode());
        voucher.showVoucher(voucher.getVoucherCode());
        voucher.showVoucher();
}
}

